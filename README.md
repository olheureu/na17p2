# na17p2 - Ombeline LHEUREUX : Le Fil électronique (16)

L'objectif du projet est de réaliser un système informatique permettant la gestion d'un journal étudiant en ligne. Le projet permettra la création d'articles, leur catégorisation, leur recherche, la gestion du suivi éditorial, et l'apposition de commentaires.

# /UPDATE/ Justification de l'UML établi pour le R

Cette V2 du projet reprend les choix de modélisation faits en repensant le sujet *à partir de* et *pour* la gestion dans la base de données.
La modélisation UML de la V1 se construisait avec pour finalité de rendre compte le plus exhaustivement possible des subtilités du sujet, notamment quant à la différenciation des comportements en fonction des rôles utilisateurs.
Ce sens de progression dans la conceptualisation ne s'ancrait pas assez sur des allers-retours *implémentation BDD - représentation UML*.

La V2 décide de produire un UML qui soit un document de travail adapté aux capacités de l'implémentation BDD, et pensé à partir de ses logiques.

Il est fait le choix de ne pas représenter d'héritage et de préférer un typage des statuts homogénéisé (dans l'UML et le MLD) : en ne retenant que les interactions premières entre les objets de notre BDD, on clarifie la lisibilité des choix d'historisation.

La V1 est toujours disponible sur le git afin de proposer une vue UML qui rend compte des droits de chaque utilisateur sur l'article, en fonction du statut de ce dernier.
La V2 se saisit du sens d'une gestion par droits utilisateurs dans la BDD et ne distinguera en pratique que dans l'implémentation SQL ces autorisations de comportements.

### Acteurs

Le système comportera les acteurs suivants :

    Administrateur  : en charge de la gestion du système (création de comptes...)

    Rédacteur : en charge de la production des articles

    Éditeur : en charge de la relecture, de la validation et de la publication des articles (les éditeurs sont regroupés au sein du comité éditorial)

    Lecteur : utilisateur final du système, pouvant mettre des commentaires ou des notes sur les articles lus

    Modérateur : en charge du suivi des commentaires et notes posées par les utilisateurs

### Besoins Auteur

    Créer un nouvel article : Un article contient des blocs, ces blocs contiennent un titre et un texte ou bien un titre et une image (il n'y aura pas de gestion de mise en forme avancée : mise en gras, positionnement des images, etc.)

    Voir ses articles en cours de rédaction, supprimer un article, récupérer un article supprimé

    Soumettre un article au comité éditorial, l'article est alors dans un état soumis et il n'est plus modifiable.

### Besoins Éditeur

    Voir l'ensemble des articles, par auteur, par date, par statut (voir ci-après)

    Accéder aux articles et les corriger

    Associer un statut à chaque article : en rédaction (les articles non soumis), soumis (statut initial donné par l'auteur), en relecture (relecture commencée par un éditeur), rejeté (associer une justification), à réviser (ajouter des préconisations), validé (il est publiable sur le site Web)

    Ajouter des mots-clés aux articles (indexation)

    Créer une arborescence de rubriques et sous-rubriques

    Associer les articles à une ou plusieurs rubriques et/ou sous-rubriques

    Sélectionner parmi les articles validés ceux qui sont publiés sur le site

    Proposer une catégorie "à l'honneur"

    Lier des articles entre eux (associer deux articles)

### Besoins Lecteurs

    Visualiser les articles

    Accéder aux articles par rubrique et/ou sous-rubrique

    Effectuer une recherche par mots clés

    Accéder à la catégorie à l'honneur

    Accéder aux articles liés depuis un article lu

    Associer un commentaire à un article (un commentaire est composé d'un titre et de texte)

    Supprimer ses commentaires

    Lire les commentaires (non masqués) des autres (voir ci-après)

### Besoins Modérateur

    Masquer un commentaire

    Supprimer un commentaire

    Mettre en exergue un commentaire

### Historisation et traçage

    On modélisera un système permettant de garder la trace de l'ensemble des articles et commentaires supprimés.

    Par ailleurs les actions des auteurs, modérateurs et éditeurs doivent être enregistrées (on veut savoir qui a rédigé un article, qui l'a validé, qui a masqué un commentaire...).