# MLD - Gestion d'un journal étudiant en ligne

### Justification des héritages

- On choisit un héritage par référence au niveau de la classe Utilisateur. En effet, bien que la classe mère soit abstraite et l'héritage exclusif, la classe mère est liée à une relation 1..\*:N avec la classe fille Administrateur.

- On choisit un héritage par classe mère au niveau de la classe Commentaire. En effet, l'héritage est exclusif et il n'y a aucune association au niveau de la classe mère.

- On choisit un héritage par référence au niveau de la classe Article. En effet, bien que la classe mère soit abstraite et l'héritage exclusif,
on ne choisit pas l'héritage par classes filles car la classe mère est liée à une relation N:M avec la classe Mot_Cle. On ne choisit pas non plus une absorption par classe mère car l'héritage est non-complet.

- On choisit un héritage par classe mère au niveau de la classe Rubrique. En effet, la classe mère n'est pas abstraite, il n'y a qu'une classe fille et l'héritage est complet.

- On choisit un héritage par classes filles au niveau de la classe Bloc. En effet, la classe mère est abstraite et l'héritage n'est pas complet.


### Tables

#### Utilisateurs
```
Utilisateur (
  #pseudo : string,
  mdp : string {not null},
  email : string,
  statut : {etudiant_Ecole | salarie_Ecole | etudiant_Autre | Autre}
)
```

```
Redacteur (
  #pseudo => Utilisateur,
  nb_articles_publies : int
)
```

```
Editeur (
  #pseudo => Utilisateur,
  specialisation : string,
  comite_editorial => Comite_Editorial
)
```

```
Administrateur (
  #pseudo => Utilisateur
)
```
```
Moderateur (
  #pseudo => Moderateur
)
```

```
Lecteur (
  #pseudo => Lecteur
)

```
```
Gestion_Utilisateur (
  #utilisateur => Utilisateur,
  #administrateur => Utilisateur
)
```

#### Historique
```
Historique_Article (
  #ref_action : integer,
  #code_article => Article,
  date_action : date

)
```
/*
```
Historique_Commentaire (
  #ref_action : integer,
  #code_commentaire => Commentaire,
  date_action : date
  )
```*/

#### Commentaire

```
Commentaire (
  #code_commentaire : integer,
  #code_action => Historique,
  titre : string,
  contenu : string,
  type : {masque | supprime | publie} {not null}
  exergue : bool
  lecteur => Lecteur,
  moderateur => Moderateur
)
```

#### Article
```
Article (
  #code_article : integer,
  titre : string,
  redacteur => Redacteur
) avec redacteur KEY
```
##### Statuts de l'article
```
Article_en_Redaction (
  #article => Article.code_article,
  #code_action => Article.code_action,
  preconisations : string
  aReviser : bool
)
```
```
Article_en_Edition (
  #article => Article.code_article,
  #code_action => Article.code_action,
  editeur => Editeur,
  enRelecture : bool
) avec editeur KEY
```
```
Article_Pret (
  #article => Article.code_article,
  #code_action => Article.code_action,
  editeur => Editeur,
  estPublie : bool
) avec editeur KEY
```
```
Article_Archive (
  #article => Article.code_article,
  #code_action => Article.code_action,
  editeur => Editeur,
  justification : string,
  estRejete : bool,
  estSupprime : bool
) avec editeur KEY
```
#### Catégorisation

```
Mot_Cle (
  #label : string
)
```

```
Indexation (
  #mot_cle => Mot_Cle,
  #article => Article
)
```

```
Lien (
  #referant => Article,
  #refere => Article
)
```

```
Rubrique (
  #titre : string
  type = {Rubrique, Categorie_Honneur}
)
```
```
Sous_Rubrique (
  #rubrique => Rubrique,
  #titre : string
)
```
```
Appartient_Rubrique (
  #rubrique => Rubrique,
  #sous_rubrique => Rubrique,
  #article => Article
)
```

##### Contenu article
```
Bloc_Texte (
  #article => Article,
  #titre : string,
  texte : string {not null}
)
```
```
Bloc_Image (
  #article => Article,
  #titre ; string,
  lien_image : string {not null}
)
```


### Contraintes

#### Contrainte de cardinalité minimale
```
Projection(Administrateur, pseudo) = Projection(Gestion_Utilisateur, administrateur)
```

#### Héritage par référence avec classe mère Utilisateur
Classe mère abstraite :
```PROJECTION (Utilisateur, pseudo)
  = UNION (
    PROJECTION (Redacteur, pseudo),
    PROJECTION (Editeur, pseudo),
    PROJECTION (Administrateur, pseudo),
    PROJECTION (Moderateur, pseudo),
    PROJECTION (Lecteur, pseudo))
```
Héritage exclusif :
```
INTERSECTION (
  PROJECTION(Redacteur,pseudo),
  PROJECTION(Editeur,pseudo),
  PROJECTION(Administrateur,pseudo),
  PROJECTION(Moderateur,pseudo),
  PROJECTION(Lecteur,pseudo))
= {}
```
#### Héritage par classe mère avec classe mère abstraite Commentaire :
```
type NOT NULL
NOT (type = masque AND exergue)
NOT (type = supprime AND exergue)
```

#### Héritage par référence avec classe mère Article :
Classe mère abstraite :
```PROJECTION(Article, code_article, code_action)
  = UNION (
    PROJECTION(Article_en_Redaction, article, code_action)  PROJECTION(Article_en_Edition, article, code_action)
    PROJECTION(Article_Pret, article, code_action)  PROJECTION(Article_Archive, article, code_action))
```
Héritage exclusif :
```
INTERSECTION (
  PROJECTION(Article_en_Redaction, article, code_action), PROJECTION(Article_en_Edition, article, code_action),
  PROJECTION(Article_Pret, article, code_action), PROJECTION(Article_Archive, article, code_action))
= {}
```

#### Héritage par classe mère avec classe mère Rubrique :
```
type NOT NULL
```

#### Commentaire
```
moderateur NOT NULL AND type = 'M' OR type = 'S'
```
#### Contrainte statuts de l'article

**Article_en_Redaction**
```
aReviser IS FALSE AND preconisations IS NULL || aReviser IS TRUE AND preconisations IS NOT NULL
```
