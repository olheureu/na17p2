# MLD - Gestion d'un journal étudiant en ligne

### Justification de l'UML établi pour le R
Cette V2 du projet reprend les choix de modélisation faits en repensant le sujet *à partir de* et *pour* la gestion dans la base de données.
La modélisation UML de la V1 se construisait avec pour finalité de rendre compte le plus exhaustivement possible des subtilités du sujet, notamment quant à la différenciation des comportements en fonction des rôles utilisateurs.
Ce sens de progression dans la conceptualisation ne s'ancrait pas assez sur des allers-retours *implémentation BDD - représentation UML*.

La V2 décide de produire un UML qui soit un document de travail adapté aux capacités de l'implémentation BDD, et pensé à partir de ses logiques.

Il est fait le choix de ne pas représenter d'héritage et de préférer un typage des statuts homogénéisé (dans l'UML et le MLD) : en ne retenant que les interactions premières entre les objets de notre BDD, on clarifie la lisibilité des choix d'historisation.

La V1 est toujours disponible sur le git afin de proposer une vue UML qui rend compte des droits de chaque utilisateur sur l'article, en fonction du statut de ce dernier.
La V2 se saisit du sens d'une gestion par droits utilisateurs dans la BDD et ne distinguera en pratique que dans l'implémentation SQL ces autorisations de comportements.

### Tables

#### Utilisateurs
```
Utilisateur (
  #pseudo : string,
  role : typeUtilisateur {not null},
  mdp : string {not null},
  email : string {not null},
  statut : statutUtilisateur {not null},
  specialisation : string,
  date_inscription : date {not null}
)
```
#### Article
```
Article (
  #code_article : integer,
  titre : string {not null},
  statut : typeArticle {not null},
  auteur => Utilisateur {not null}
)
```

#### Annotation
```
Annotation (
  #code_annotation : integer {key},
  #article => Article(code_article),
  titre : string {not null},
  contenu_commentaire : string,
  note_article : integer,
  statut : typeCommentaire {not null}
  exergue : bool
)
```

#### Historique
```
Action_Article (
  #ref_action : integer,
  #article => Article,
  utilisateur => Utilisateur {not null},
  intitule : typeAcArticle,
  date_action : date {not null},
  explication : text
)
```
```
Action_Annotation (
  #ref_action : integer,
  #annotation => Annotation(code_annotation),
  #article => Annotation(article),
  utilisateur => Utilisateur {not null},
  intitule : typeAcAnnotation {not null},
  date_action : date {not null},
  explication : text
  )
```

#### Catégorisation

```
Mot_Cle (
  #label : string
)
```

```
Indexation (
  #mot_cle => Mot_Cle,
  #article => Article
)
```

```
Lien (
  #referant => Article,
  #refere => Article
)
```

On choisit un héritage par classe mère au niveau de la classe Rubrique. En effet, la classe mère n'est pas abstraite, il n'y a qu'une classe fille et l'héritage est complet.

```
Rubrique (
  #titre : string DEFAULT 'A l"honneur'
  type = {Rubrique, Categorie_Honneur}
)
```
```
Sous_Rubrique (
  #rubrique => Rubrique,
  #titre : string
)
```

```
Appartient_Rubrique (
  #code_appartenance : integer,
  #article => Article,
  rubrique => Sous_Rubrique.rubrique {not null},
  sous_rubrique => Sous_Rubrique.titre
)
```

##### Contenu article

On choisit un héritage par classes filles au niveau de la classe Bloc. En effet, la classe mère est abstraite et l'héritage n'est pas complet.

```
Bloc_Texte (
  #article => Article,
  #titre : string,
  texte : text {not null}
)
```

```
Bloc_Image (
  #article => Article,
  #titre : string,
  lien_image : string {not null}
)
```

### Contraintes

##### Annotation
```
NOT (contenu is NULL AND note is NULL)
```
```
type NOT NULL
NOT (typeCommentaire = masque AND exergue)
NOT (typeCommentaire = supprime AND exergue)
```

Contrainte de cardinalité minimale :
```
Projection(Annotation, article) = Projection(Article, code_article)
```

#### Article (contrainte de cardinalité minimale)
```
Projection(Article, auteur) = Projection(Utilisateur, pseudo)
```

#### Rubrique :
```
type NOT NULL
```
```
type='C_H' AND titre='A l"honneur'
```


#### Appartient_Rubrique

```
PROJECTION(Appartient_Rubrique, sous_rubrique)
= PROJECTION (Sous_Rubrique, rubrique)
  UNION
PROJECTION(Sous_Rubrique, rubrique)
= PROJECTION(Appartient_Rubrique, rubrique)
```
