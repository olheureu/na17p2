DROP TABLE IF EXISTS Utilisateur cascade;
DROP TABLE IF EXISTS Redacteur cascade;
DROP TABLE IF EXISTS Editeur cascade;
DROP TABLE IF EXISTS Administrateur cascade;
DROP TABLE IF EXISTS Moderateur cascade;
DROP TABLE IF EXISTS Lecteur cascade;
DROP TABLE IF EXISTS Gestion_utilisateur cascade;
DROP TABLE IF EXISTS Historique cascade;
DROP TABLE IF EXISTS Commentaire cascade;
DROP TABLE IF EXISTS Article cascade;
DROP TABLE IF EXISTS Article_en_Redaction cascade;
DROP TABLE IF EXISTS Article_en_Edition cascade;
DROP TABLE IF EXISTS Article_Pret cascade;
DROP TABLE IF EXISTS Article_Archive cascade;
DROP TABLE IF EXISTS Indexation cascade;
DROP TABLE IF EXISTS Rubrique cascade;
DROP TABLE IF EXISTS Sous_Rubrique cascade;
DROP TABLE IF EXISTS Appartient_Rubrique cascade;
DROP TABLE IF EXISTS Proposition_Categorie_Honneur cascade;
DROP TABLE IF EXISTS Bloc_Texte cascade;
DROP TABLE IF EXISTS Bloc_Image cascade;

CREATE TYPE type_utilisateur AS ENUM('etudiant_Ecole', 'salarie_Ecole', 'etudiant_Autre', 'Autre');
CREATE TYPE type_commentaire AS ENUM ('M', 'S', 'P');
CREATE TYPE type_rubrique AS ENUM ('R','C_H');

CREATE TABLE Utilisateur (
    pseudo VARCHAR(30) PRIMARY KEY,
    mdp VARCHAR(30) NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL,
    statut type_utilisateur NOT NULL
);

CREATE TABLE Redacteur (
  pseudo VARCHAR(30) PRIMARY KEY,
  nb_articles_publies INTEGER,
  FOREIGN KEY (pseudo) REFERENCES Utilisateur(pseudo)
);

CREATE TABLE Editeur (
  pseudo VARCHAR(30) PRIMARY KEY,
  specialisation VARCHAR(50),
  FOREIGN KEY (pseudo) REFERENCES Utilisateur(pseudo)
);

CREATE TABLE Administrateur (
  pseudo VARCHAR(30) PRIMARY KEY,
  FOREIGN KEY (pseudo) REFERENCES Utilisateur(pseudo)
);

CREATE TABLE Moderateur (
  pseudo VARCHAR(30) PRIMARY KEY,
  FOREIGN KEY (pseudo) REFERENCES Utilisateur(pseudo)
);

CREATE TABLE Lecteur (
  pseudo VARCHAR(30) PRIMARY KEY,
  FOREIGN KEY (pseudo) REFERENCES Utilisateur(pseudo)
);

CREATE TABLE Gestion_Utilisateur (
  utilisateur VARCHAR(30),
  administrateur VARCHAR(30),
  FOREIGN KEY (utilisateur) REFERENCES Utilisateur(pseudo),
  FOREIGN KEY (administrateur) REFERENCES Administrateur(pseudo)
);

CREATE TABLE Historique (
  ref_action INTEGER PRIMARY KEY,
  date_action DATE UNIQUE NOT NULL
);

CREATE TABLE Commentaire (
  code_commentaire INTEGER,
  code_action INTEGER,
  titre VARCHAR(30),
  contenu VARCHAR NOT NULL,
  type type_commentaire NOT NULL,
  exergue BOOLEAN,
  lecteur VARCHAR(30) UNIQUE NOT NULL,
  moderateur VARCHAR(30),
  FOREIGN KEY (code_action) REFERENCES Historique(ref_action),
  FOREIGN KEY(lecteur) REFERENCES Lecteur(pseudo),
  FOREIGN KEY(moderateur) REFERENCES Moderateur(pseudo),
  PRIMARY KEY (code_commentaire, code_action)
);

CREATE TABLE Article (
  code_article INTEGER PRIMARY KEY,
  titre VARCHAR(100) NOT NULL,
  redacteur VARCHAR(30) NOT NULL UNIQUE,
  FOREIGN KEY (redacteur) REFERENCES Redacteur(pseudo)
);

CREATE TABLE Article_en_Redaction (
  article INTEGER,
  code_action INTEGER,
  preconisations VARCHAR,
  aReviser BOOLEAN,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  FOREIGN KEY (code_action) REFERENCES Historique(ref_action)
);

CREATE TABLE Article_en_Edition (
  article INTEGER,
  code_action INTEGER,
  editeur VARCHAR(30) NOT NULL UNIQUE,
  enRelecture BOOLEAN,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  FOREIGN KEY (code_action) REFERENCES Historique(ref_action)
);

CREATE TABLE Article_Pret (
  article INTEGER,
  code_action INTEGER,
  editeur VARCHAR(30) NOT NULL UNIQUE,
  estPublie BOOLEAN,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  FOREIGN KEY (code_action) REFERENCES Historique(ref_action)
);

CREATE TABLE Article_Archive (
  article INTEGER,
  code_action INTEGER,
  justification VARCHAR,
  editeur VARCHAR(30) NOT NULL UNIQUE,
  estRejete BOOLEAN,
  estSupprime BOOLEAN,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  FOREIGN KEY (code_action) REFERENCES Historique(ref_action)
);

CREATE TABLE Mot_Cle (
  label VARCHAR(30) PRIMARY KEY
);

CREATE TABLE Indexation (
  mot_cle VARCHAR(30),
  article INTEGER,
  FOREIGN KEY (mot_cle) REFERENCES Mot_Cle(label),
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY(mot_cle, article)
);

CREATE TABLE Lien (
  referant INTEGER,
  refere INTEGER,
  FOREIGN KEY (referant) REFERENCES Article(code_article),
  FOREIGN KEY (refere) REFERENCES Article(code_article),
  PRIMARY KEY (referant, refere)
);

CREATE TABLE Rubrique (
  titre VARCHAR(40) PRIMARY KEY,
  type type_rubrique,
  editeur VARCHAR(30),
  FOREIGN KEY (editeur) REFERENCES Editeur(pseudo)
);

CREATE TABLE Sous_Rubrique (
  rubrique VARCHAR(40),
  titre VARCHAR(40),
  FOREIGN KEY (rubrique) REFERENCES Rubrique(titre),
  PRIMARY KEY (rubrique, titre)
);

CREATE TABLE Appartient_Rubrique (
  rubrique VARCHAR(40),
  article INTEGER,
  FOREIGN KEY (rubrique) REFERENCES Rubrique(titre),
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY (rubrique, article)
);

CREATE TABLE Bloc_Texte (
  article INTEGER,
  titre VARCHAR(50),
  texte VARCHAR NOT NULL,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY (article, titre)
);

CREATE TABLE Bloc_Image (
  article INTEGER,
  titre VARCHAR(50),
  lien_image VARCHAR NOT NULL,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY (article, titre)
);

/* JEU DE TEST*/

INSERT INTO Utilisateur (pseudo, mpd, email, statut)
VALUES ('Anouk', 'chaton36', 'anouk.cho@etu.utc.fr', 'etudiant_Ecole'),
('Anne McCaffrey', '!cafeouthe?', 'anne.sou@etu.utc.fr', 'etudiant_Ecole'),
('Poetedu60', 'rAgOnDiN', 'adrien.cha@etu.utc.fr', 'etudiant_Ecole'),
('Apollin''hair', 'tapisRouge', 'felix.pou@etu.utc.fr', 'etudiant_Ecole'),
('Ingé d''Erable', 'CET', 'pierre.kid@etu.utc.fr', 'etudiant_Ecole'),
('Marseille', 'teamchat', 'aline.zha@etu.utc.fr' , 'etudiant_Ecole'),
('42', 'bison ravi', 'valentine.ver@etu.utc.fr', 'etudiant_Ecole'),
('Jojo', 'sire_youarille', 'joris.pla@etu.utc.fr', 'etudiant_Ecole'),
('GROOOS', 'lamoulaga', 'celestin.gar@etu.utc.fr', 'etudiant_Ecole'),
('Neptune', 'princessetajine', 'sarah.mig@hotmail.fr', 'etudiant_Autre'),
('Petit prince', 'titoune', 'mai-anh.dan@sorbonnes-universite.fr', 'etudiant_Autre'),
('Totor', 'orteilislifeorteilislove', 'bulldozerdu75@pigalle.fr', 'etudiant_Autre'),
('Enactboy', 'autopoiesesomatosensorielle', 'charles.len@utc.fr', 'salarie_Ecole'),
('Spam man', '^^', 'nicolas.sal@utc.fr', 'salarie_Ecole' ),
('Artisan de demain', 'passionbiface', 'guillaume.car@utc.fr', 'salarie_Ecole'),
('Croziflette', 'AA', 'stephane.cro@utc.fr', 'salarie_Ecole'),
('Rouge glotte', '*socrate22*', 'cleo.col@utc.fr', 'salarie_Ecole'),
('M.Gaston', 'allezlesjeunesonyva', 'salarie_Ecole'),
('Deleuze', 'fuitedeau', 'gilles.deleuze@lilo.org', 'Autre'),
('Foucault', 'surveilleretpetrir', 'michel.foucault@lilo.org', 'Autre'),
('Lordon', 'affect_choucroute', 'frederic.lordon@protonmail.com', 'Autre'),
('Dame à oiseau', 'lahordedelacontrebourrasque', 'technococon@noidentity.org', 'Autre'),
('VD', 'gorilletheory', 'virginie.despentes@wanadoo.fr', 'Autre'),
('Barbier de Belleville', 'cestmoicest','serge.reggiani@hotmail.fr', 'Autre'),
('Ali', 'millefeuilles', 'alice.zeniter@utc.fr', 'Autre'),
('Petit bonhomme', 'bancspublics', 'georges.brassens@lilo.org', 'Autre'),
('Chercheur de fecondité', 'XxmagienoirexX', 'antonin.artaud@bbox.fr', 'Autre');

INSERT INTO Redacteur (pseudo)
VALUES ('Anouk'),
('Lordon'),
('Artisan de demain'),
('Anne McCaffrey'),
('Petit bonhomme'),
('Apollin''hair'),
('Croziflette');

INSERT INTO Editeur (pseudo, specialisation)
VALUES ('Deleuze', 'SF, Sports de raquette'),
('Ali', 'Diplomatie internationale'),
('Jojo', 'Programmation, Réalité virtuelle');
('Poetedu60', 'Chevalerie medievale'),
('Spam man', 'Jazz''n Soul, Clinique du travail'),
('Dame à oiseau', 'Sociétés de traces');


INSERT INTO Moderateur (pseudo)
VALUES ('Foucault'),
('VD'),
('Ingé d''Erable'),
('M.Gaston');

INSERT INTO Lecteur(pseudo,statut)
VALUES ('Rougle glotte'),
('Barbier de Belleville'),
('Petit prince'),
('Marseille'),
('Enactboy'),
('Neptune'),
('GROOOS'),
('Totor');

INSERT INTO Administrateur (pseudo)
VALUES ('42');

INSERT INTO Article(code_article, titre, redacteur)
VALUES (1,'Nos solitudes du 21ème siècle', 'Anouk'),
(2, 'Sortir les parasols', 'Lordon'),
(3, 'Adaptation-exaptation : pour une délinéarisation de l''évolution', 'Artisan de demain'),
(4, 'Anne McCaffrey', 'La Ballade de Pern');

INSERT INTO Article_en_Redaction(article, code_action, preconisations, aReviser)
VALUES (1, 1,'', 0),

INSERT INTO
