# Note de clarification

## Description du projet

L'objectif du projet est de réaliser un système informatique permettant la gestion d'un journal étudiant en ligne.

Le projet doit permettre :
- la création d'articles
- la catégorisation des articles en rubriques, sous-rubriques et mots-clés
- la recherche des articles par rubriques, sous-rubriques, mots-clés, articles associés et données de l'article (contenu, rédacteur, date)
- la gestion du suivi éditorial des articles
- l'apposition de commentaires
- l'historisation des actions permises par les rôles de rédacteur, d'éditeur et de modérateur
- la mémorisation des articles et commentaires supprimés

La modélisation doit rendre compte des étapes du cycle de vie d'un article (la *rédaction*, l'*édition*, la *publication*, l'*archivage*, la *lecture*, les *retours*, la *modération* des retours) et par rebond les acteurs intervenants. Un historique du contenu produit est conservé et un traçage des actions effectuées est assuré.

## Objets nécessaires à la modélisation

### Utilisateurs (fonctions)
#### Rédacteur
- possède un pseudonyme
- possède un mot-de-passe
- possède un email
- possède un nombre d'articles publiés sur le site
- peut créer un nouvel article
- peut voir ses articles en cours de rédaction
- peut supprimer un article qu'il a rédigé
- peut récupérer un article qu'il a supprimé
- peut soumettre un article au comité éditorial


#### Administrateur
- possède un pseudonyme
- possède un mot-de-passe
- possède un email
- peut créer les comptes utilisateurs
- peut associer le rôle d'éditeur, d'auteur, de modérateur ou de lecteur à chaque compte utilisateur
- peut supprimer les comptes utilisateurs

#### Editeur
- possède un pseudonyme
- possède un mot-de-passe
- possède un email
- peut accéder à tous les articles
- peut associer un statut à tous les articles
- peut ajouter des mots-clés aux articles
- peut créer une arborescence de rubriques et sous-rubriques
- peut associer les articles à une ou plusieurs rubriques et/ou sous-rubriques
- peut sélectionner parmi les articles validés ceux qui sont publiés sur le site
- peut proposer une catégorie "à l'honneur"
- peut ajouter des articles à la catégorie "à l'honneur"
- peut associer deux articles ensemble

#### Lecteur
- possède un pseudonyme
- possède un mot-de-passe
- possède un email
- possède un statut de lecteur *(étudiant de l'école à l'initiative du journal, salarié de l'école, étudiant d'une autre école/université, lecteur autre)*
- possède une date d'inscription sur la plateforme
- peut visualiser les articles publiés sur le site
- peut accéder aux articles par rubrique et/ou sous-rubrique
- peut effectuer une recherche par mots clés
- peut accéder à la catégorie à l'honneur
- peut accéder aux articles liés depuis un article lu
- peut commenter un article
- peut supprimer ses commentaires
- peut lire les commentaires non masqués des autres

#### Modérateur
- possède un pseudonyme
- peut masquer un commentaire
- peut supprimer un commentaire
- peut mettre en exergue un commentaire

### Objets manipulés  (propriétés et contraintes)

#### Article
- possède un titre
- possède un statut *en rédaction*, *soumis*, *en relecture*, *rejeté*, *à réviser* ou *validé*
- possède un état *archivé* vrai ou faux
- possède un mot-clé
- possède une date
- peut être supprimé

#### Bloc d'un article
- possède un titre et une image ou un titre et un texte

#### Rubrique
- possède un titre

#### Sous-rubrique
- possède un titre

#### Catégorie "à l'honneur" (excusez du peu)

#### Commentaire d'un article
- possède un en-tête
- possède un texte
- possède une date
- peut être masqué
- peut être supprimé
- peut être distingué

## Hypothèses faites pour la modélisation

### Utilisateurs
- Les différents utilisateurs de la base de donnée sont identifiés en ligne par un pseudonyme.
- Un utilisateur ne peut pas cumuler les rôles. Il est *rédacteur*, *lecteur*, *administrateur*, *editeur* ou *modérateur*.
- On associe à chaque action l'identifiant de l'utilisateur qui en est à l'origine.   

### Rédacteur
- Un rédacteur ne peut supprimer / récupérer que les articles qu'il a rédigé.

### Lecteur
- Tous les lecteurs ont accès à tous les articles publiés (pas de hiérarchie d'accès type tarif abonné).

### Administrateur
- Il peut y avoir plusieurs administrateurs du site.

### Editeur
- Une fois l'article validé par l'Editeur, il n'est plus modifiable. Il doit repasser en relecture pour être modifié.

### Comité éditorial
- Le choix d'un éditeur ne doit pas être précédé d'une consultation du comité éditorial. Les éditeurs travaillent en autonomie.
-  Le choix d'un éditeur sur le statut d'un article peut être contesté en aval par un autre éditeur. La proposition de ce dernier doit alors obtenir le soutien de la majorité - 1 du comité éditorial.

### Article
- Un article n'est rédigé que par un seul auteur.
- La date de l'article correspond à la dernière modification de l'article.
- Les données d'un article supprimé sont conservées dans la base de données. On dit alors qu'un article supprimé n'apparait pas sur le site mais qu'il est **archivé** dans les données du système de gestion.

#### Changements de statut d'un article
  - Un article n'a, à un instant t, qu'un seul statut (les états du statut sont exclusifs).

  ##### Archivage d'un article
    - Un article est archivé s'il a été supprimé par son auteur ou rejeté par un éditeur :
      - Seul un article existant peut être archivé.
      - Un article peut être supprimé par son auteur n'importe quand, c'est-à-dire avec n'importe quel statut *(en rédaction, soumis, en relecture, à réviser, validé, publié)*.
      - Un article ne peut être rejeté par un éditeur que si l'éditeur était *en relecture* de l'article.
      - Un article archivé peut être récupéré par son auteur. L'article contient alors tout le contenu Son comportement est alors analogue à un article que viendrait de créer l'auteur: l'article passe en rédaction.

    ##### Ordre d'édition
    - La chronologie suivante des états du statut d'un article {*en rédaction > soumis > en relecture > à réviser > validé > publié*} est unidirectionnellement contraignante, c'est-à-dire :
      - Un article ne peut passer d'un état antérieur qu'à celui directement suivant.
      - Un article peut passer d'un état postérieur à n'importe quel état antérieur.
      - L'évolution d'un état à un autre d'un article ne peut se faire que si l'utilisateur possède les droits associés, précisés dans *Objets nécessaires à la modélisation*.


### Commentaire
- Un commentaire masqué n'est visible que par son auteur alors qu'un commentaire supprimé n'est visible par aucun des lecteurs.
- Un commentaire n'a pas à être validé par un modérateur avant d'être publié par un lecteur : son contenu est directement visible sur le site. Son état peut a posteriori être changé par un modérateur *(masqué ou supprimé)*.
- Un commentaire doit être publié pour être mis en exergue.
- Un commentaire masqué ou supprimé n'est plus mis en exergue s'il l'était.

#### Catégorisation
- Un article peut être attaché à plusieurs rubriques et sous-rubriques.
- Un article peut ne pas être attaché à une rubrique.
- La vie de l'article est indépendante de la vie des rubriques ou des sous-rubriques auxquelles il est associé.

### Bloc
- Un bloc n'existe pas indépendamment de l'article auquel il appartient.

### Sous-rubrique et Rubrique
- Une rubrique peut ne pas avoir de sous-rubrique.
- Une sous-rubrique est attachée à une unique rubrique.
- L'existence d'une sous-rubrique dépend du cycle de vie de la rubrique à laquelle elle appartient.
