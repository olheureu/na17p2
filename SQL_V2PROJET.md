/* TABLES*/

DROP TABLE IF EXISTS Utilisateur cascade;
DROP TABLE IF EXISTS Article cascade;
DROP TABLE IF EXISTS Annotation cascade;
DROP TABLE IF EXISTS Action_Article cascade;
DROP TABLE IF EXISTS Action_Annotation cascade;
DROP TABLE IF EXISTS Mot_Cle cascade;
DROP TABLE IF EXISTS Indexation cascade;
DROP TABLE IF EXISTS Lien;
DROP TABLE IF EXISTS Rubrique cascade;
DROP TABLE IF EXISTS Sous_Rubrique cascade;
DROP TABLE IF EXISTS Appartient_Rubrique cascade;
DROP TABLE IF EXISTS Bloc_Texte cascade;
DROP TABLE IF EXISTS Bloc_Image cascade;

CREATE TYPE statutUtilisateur AS ENUM('etudiant_ecole', 'salarie_ecole', 'etudiant_autre', 'autre');

CREATE TYPE typeUtilisateur AS ENUM('redacteur','editeur','moderateur','lecteur','administrateur');

CREATE TYPE typeArticle AS ENUM('enRedaction','soumis','enRelecture','aReviser','rejete','supprime', 'valide', 'publie');

CREATE TYPE typeAcArticle AS ENUM('redige','supprime','soumet','relit','preconise','rejette', 'valide', 'publie');

CREATE TYPE typeAcAnnotation AS
ENUM ('annote', 'masque', 'supprime', 'distingue');

CREATE TYPE typeCommentaire AS ENUM ('publie', 'masque', 'supprime');

CREATE TYPE typeRubrique AS ENUM ('R','C_H');

CREATE TABLE Utilisateur (
    pseudo VARCHAR(30) PRIMARY KEY,
    role typeUtilisateur NOT NULL,
    mdp VARCHAR(30) NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL,
    statut statutUtilisateur NOT NULL,
    specialisation VARCHAR DEFAULT NULL,
    date_inscription TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Annotation (
  code_annotation INTEGER,
  article INTEGER,
  auteur VARCHAR(30) NOT NULL,
  titre VARCHAR(30) NOT NULL,
  contenu_commentaire TEXT,
  note_article INTEGER,
  statut typeCommentaire NOT NULL DEFAULT 'publie',
  exergue BOOLEAN DEFAULT false,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  FOREIGN KEY (auteur) REFERENCES Utilisateur(pseudo),
  PRIMARY KEY(code_annotation, article)
);

CREATE TABLE Action_Annotation (
  ref_action INTEGER,
  annotation INTEGER,
  article INTEGER,
  utilisateur VARCHAR(30) NOT NULL,
  intitule typeAcAnnotation NOT NULL,
  date_action TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  explication TEXT DEFAULT NULL,
  FOREIGN KEY (annotation, article) REFERENCES Annotation(code_annotation, article),
  PRIMARY KEY(ref_action,annotation, article)
);

CREATE TABLE Article (
  code_article INTEGER PRIMARY KEY,
  auteur VARCHAR(30) NOT NULL,
  titre VARCHAR(100) NOT NULL,
  statut typeArticle NOT NULL DEFAULT 'enRedaction',
  FOREIGN KEY (auteur) REFERENCES Utilisateur(pseudo)
);

CREATE TABLE Action_Article (
  ref_action INTEGER,
  article INTEGER,
  utilisateur VARCHAR(30) NOT NULL,
  intitule typeAcArticle NOT NULL,
  date_action TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  explication TEXT DEFAULT NULL,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY(ref_action,article)
);

CREATE TABLE Mot_Cle (
  label VARCHAR(30) PRIMARY KEY
);

CREATE TABLE Indexation (
  mot_cle VARCHAR(30),
  article INTEGER,
  FOREIGN KEY (mot_cle) REFERENCES Mot_Cle(label),
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY(mot_cle, article)
);

CREATE TABLE Lien (
  referant INTEGER,
  refere INTEGER,
  FOREIGN KEY (referant) REFERENCES Article(code_article),
  FOREIGN KEY (refere) REFERENCES Article(code_article),
  PRIMARY KEY (referant, refere)
);

CREATE TABLE Rubrique (
  titre VARCHAR(40) PRIMARY KEY DEFAULT 'A l"honneur',
  type typeRubrique
);

CREATE TABLE Sous_Rubrique (
  rubrique VARCHAR(40),
  titre VARCHAR(40),
  FOREIGN KEY (rubrique) REFERENCES Rubrique(titre),
  PRIMARY KEY (rubrique, titre)
);

CREATE TABLE Appartient_Rubrique (
  code_appartenance INTEGER,
  article INTEGER,
  rubrique VARCHAR(40) NOT NULL,
  sous_rubrique VARCHAR(40) DEFAULT NULL,
  FOREIGN KEY (rubrique, sous_rubrique) REFERENCES Sous_Rubrique(rubrique, titre),
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY (article, code_appartenance)
);

CREATE TABLE Bloc_Texte (
  article INTEGER,
  titre VARCHAR(50),
  texte TEXT NOT NULL,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY (article, titre)
);

CREATE TABLE Bloc_Image (
  article INTEGER,
  titre VARCHAR(50),
  lien_image VARCHAR NOT NULL,
  FOREIGN KEY (article) REFERENCES Article(code_article),
  PRIMARY KEY (article, titre)
);

/* VUES*/

//Tous les auteurs avec nombre articles crees et nombre articles publies et note moyenne des articles

  CREATE VIEW Redacteurs AS
  SELECT DISTINCT ON (U.pseudo) U.pseudo,
    extract(day from CURRENT_TIMESTAMP-U.date_inscription)+1 AS "jours d'ancienneté",
    COALESCE((SELECT COUNT(A.code_article) WHERE U.pseudo = A.auteur),0) AS "nombre d'articles",
    COALESCE((SELECT COUNT(A.code_article) WHERE A.auteur= U.pseudo AND A.statut='publie'), 0) AS "nombre d'articles publiés",
	CAST(AVG(AN.note_article) AS FLOAT) AS "note moyenne"
  FROM Utilisateur AS U
  LEFT JOIN (Article AS A
  INNER JOIN Annotation AS AN ON A.code_article=AN.article)
  ON U.pseudo= A.auteur
  WHERE U.role = 'redacteur'
  GROUP BY U.pseudo, A.auteur, A.statut, AN.auteur;

// Tous les éditeurs avec nombre d'articles en relecture

CREATE VIEW Editeurs AS
SELECT DISTINCT ON (pseudo)
  U.pseudo,
  extract(day from CURRENT_TIMESTAMP-U.date_inscription)+1 AS  "jours d'ancienneté",
  COALESCE((SELECT COUNT(A.code_article) WHERE A.statut = 'enRelecture'),0) AS "nombre d'articles en relecture"
  FROM Utilisateur AS U
  LEFT JOIN (Action_Article AS AA
INNER JOIN Article AS A ON AA.article=A.code_article)
ON U.pseudo=AA.utilisateur
WHERE U.role = 'editeur'
GROUP BY U.pseudo, AA.utilisateur, AA.article, AA.intitule, A.statut;

// Tous les lecteurs avec nombre annotations + nombre commentaires masques ou supprimes
+ nombre commentaires distingues

CREATE VIEW Lecteurs AS
SELECT DISTINCT ON (pseudo)
		U.pseudo,
		extract(day from CURRENT_TIMESTAMP-U.date_inscription)+1 AS  "jours d'ancienneté",
		COALESCE((SELECT COUNT(AAN.annotation) WHERE AAN.intitule= 'annote'),0) AS "nombre d'annotations",
		COALESCE((SELECT COUNT(AN.code_annotation) WHERE AN.auteur=U.pseudo AND (AN.statut='supprime' OR AN.statut='masque')),0) AS "commentaires masqués/supprimés",
		COALESCE((SELECT COUNT(AN.code_annotation) WHERE AN.auteur=U.pseudo AND AN.exergue is true), 0) AS "commentaires distingués"
    FROM Utilisateur AS U
	LEFT JOIN (Action_Annotation AS AAN
	INNER JOIN Annotation AS AN ON AAN.annotation = AN.code_annotation)
	ON U.pseudo=AAN.utilisateur
	WHERE U.role = 'lecteur'
  GROUP BY U.pseudo, AAN.intitule, AN.auteur, AN.statut, AN.exergue;

/* JEU DE TEST*/

INSERT INTO Utilisateur (pseudo, role, mdp, email, statut, specialisation, date_inscription)
VALUES ('Anouk', 'redacteur', 'chaton36', 'anouk.cho@etu.utc.fr', 'etudiant_ecole', DEFAULT, DEFAULT),
('Anne McCaffrey', 'redacteur', '!cafeouthe?', 'anne.sou@etu.utc.fr', 'etudiant_ecole', DEFAULT, DEFAULT),
('Apollin"hair', 'redacteur', 'tapisRouge', 'felix.pou@etu.utc.fr', 'etudiant_ecole', DEFAULT, DEFAULT),
('Artisan de demain', 'redacteur', 'passionbiface', 'guillaume.car@utc.fr', 'salarie_ecole', DEFAULT, DEFAULT),
('Croziflette', 'redacteur', 'AA', 'stephane.cro@utc.fr', 'salarie_ecole', DEFAULT, DEFAULT),
('Lordon', 'redacteur', 'affect_choucroute', 'frederic.lordon@protonmail.com', 'autre', DEFAULT, DEFAULT),
('Petit bonhomme', 'redacteur', 'bancspublics', 'georges.brassens@lilo.org', 'autre', DEFAULT, DEFAULT),
('Chercheur de fecondité', 'redacteur', 'XxmagienoirexX', 'antonin.artaud@bbox.fr', 'autre', DEFAULT, DEFAULT),

('Deleuze', 'editeur', 'fuitedeau', 'gilles.deleuze@lilo.org', 'autre', 'SF, Sports de raquette', DEFAULT),
('Dame à oiseau', 'editeur', 'lahordedelacontrebourrasque', 'technococon@noidentity.org', 'autre',  'Sociétés de traces', DEFAULT),
('Spam man', 'editeur', '^^', 'nicolas.sal@utc.fr', 'salarie_ecole', 'Jazz"n Soul, Clinique du travail', DEFAULT),
('Ali', 'editeur', 'millefeuilles', 'alice.zeniter@utc.fr', 'autre','Diplomatie internationale', DEFAULT),
('Poetedu60', 'editeur', 'rAgOnDiN', 'adrien.cha@etu.utc.fr', 'etudiant_ecole',  'Chevalerie medievale', DEFAULT),
('Jojo', 'editeur', 'sire_youarille', 'joris.pla@etu.utc.fr', 'etudiant_ecole', 'Programmation, Réalité virtuelle', DEFAULT),

('Ingé d''Erable',  'moderateur', 'CET', 'pierre.kid@etu.utc.fr', 'etudiant_ecole', DEFAULT, DEFAULT),
('M.Gaston',  'moderateur', 'allezlesjeunesonyva', 'michel.ga@utc.fr','salarie_ecole', DEFAULT, DEFAULT),
('Foucault', 'moderateur', 'surveilleretpetrir', 'michel.foucault@lilo.org', 'autre', DEFAULT, DEFAULT),
('VD',  'moderateur', 'gorilletheory', 'virginie.despentes@wanadoo.fr', 'autre', DEFAULT, DEFAULT),

('Marseille', 'lecteur', 'teamchat', 'aline.zha@etu.utc.fr' , 'etudiant_ecole', DEFAULT, DEFAULT),
('GROOOS', 'lecteur', 'lamoulaga', 'celestin.gar@etu.utc.fr', 'etudiant_ecole', DEFAULT, DEFAULT),
('Neptune', 'lecteur', 'princessetajine', 'sarah.mig@hotmail.fr', 'etudiant_autre', DEFAULT, DEFAULT),
('Petit prince', 'lecteur', 'titoune', 'mai-anh.dan@sorbonnes-universite.fr', 'etudiant_autre', DEFAULT, DEFAULT),
('Totor', 'lecteur', 'orteilislifeorteilislove', 'bulldozerdu75@pigalle.fr', 'etudiant_autre', DEFAULT, DEFAULT),
('Enactboy', 'lecteur', 'autopoiesesomatosensorielle', 'charles.len@utc.fr', 'salarie_ecole', DEFAULT, DEFAULT),
('Rouge glotte', 'lecteur', '*socrate22*', 'cleo.col@utc.fr', 'salarie_ecole', DEFAULT, DEFAULT),
('Barbier de Belleville', 'lecteur' ,'cestmoicest','serge.reggiani@hotmail.fr', 'autre', DEFAULT, DEFAULT),

('42', 'administrateur', 'bison ravi', 'valentine.ver@etu.utc.fr', 'etudiant_ecole', DEFAULT, DEFAULT);

INSERT INTO Article(code_article, titre, auteur)
VALUES (1,'Nos solitudes du 21ème siècle', 'Anouk'),
(2, 'La Ballade de Pern', 'Anne McCaffrey'),
(3,'Berceau cacophonique','Apollin"hair');

UPDATE Article SET statut='publie' WHERE code_article=2;
UPDATE Article SET statut='enRelecture' WHERE code_article=3;


INSERT INTO Action_Article (ref_action, article, utilisateur, intitule, date_action, explication)
VALUES (1, 1, 'Anouk', 'redige', '2020-05-20 10:01:13', DEFAULT),
(2, 1, 'Anouk', 'soumet', '2020-06-03 23:58:59', 'Je m"excuse pour le retard!'),
(3, 1, 'Spam man', 'relit', DEFAULT, DEFAULT),
(4, 1, 'Spam man', 'valide', DEFAULT, DEFAULT),
(5, 1, 'Spam man', 'publie', DEFAULT, DEFAULT),
(1, 2, 'Anne McCaffrey', 'redige', '2020-05-13 13:24:55', DEFAULT),
(1, 3, 'Apollin"hair', 'redige', '2020-03-16 16:45:00', DEFAULT),
(2, 3, 'Apollin"hair', 'soumet', '2020-03-16 23:18:45', DEFAULT),
(3, 3, 'Dame à oiseau', 'relit', '2020-03-16 02:45:12', DEFAULT);

INSERT INTO Annotation(code_annotation, article, auteur, titre, contenu_commentaire, note_article, statut, exergue)
VALUES (1, 1, 'Petit prince', 'Solitudes communes', 'Merci! Que ça fait du bien de sentir que cette solitude est en fin de compte bien peu solitaire.. N"y a-t-il pas plus vraie communauté que l"autarcie ? Bonne continuation.', '5', DEFAULT, DEFAULT);

INSERT INTO Action_Annotation(ref_action, annotation, article, utilisateur, intitule, date_action, explication)
VALUES (1, 1, 1, 'Petit prince', 'annote', DEFAULT, DEFAULT),
(2, 1, 1, 'M.Gaston', 'distingue', DEFAULT, DEFAULT);

INSERT INTO Rubrique (titre, type)
VALUES ('Culture', 'R'),
(DEFAULT, 'C_H');

INSERT INTO Sous_Rubrique(rubrique, titre)
VALUES ('Culture','Théâtre');

INSERT INTO Appartient_Rubrique(code_appartenance, article, rubrique, sous_rubrique)
VALUES(1, 1 ,'Culture', 'Théâtre'),
(2, 1, 'A l"honneur', DEFAULT);

INSERT INTO Mot_Cle(label)
VALUES('chronique'),
('fiche de lecture');

INSERT INTO Indexation(mot_cle, article)
VALUES ('chronique','1'),
('fiche de lecture', '2');

INSERT INTO Lien(referant,refere)
VALUES(1, 2);

INSERT INTO Bloc_Texte(article, titre, texte)
VALUES(1, 'Intro', 'Deux mois de vide c"est original. En fait, cela ne fait pas vraiment deux mois. Tout d"abord, parce que les premières semaines se superposent à une vitesse folle. Une vélocité dont on n"a pas idée. Tout, tout est tellement cadré. Tout est si organisé. Tout est si précis, que chaque chose est presque déjà vécue avant d"apparaître. Syndrome du 21è siècle ? <\br>
Mais au début de la cinquième semaine, lorsque les vacances sont officiellement terminées, chacun se replace dans sa bulle de confort, de routine étudiante où les jours ne se ressemblent pas. Alors, comme les directions se dispersent, on se retrouve soudain, à avoir du temps. Eux, ils reprennent.'),
(1, 'P1', 'Moi, dorénavant je peux simplement prendre ce qui vient, ce qui arrive. Je peux flirter avec les livres, Loin d"Alexis Michalik, pour rêver du voyage et de l"apport que ce dernier à sur chacun. Sur le bénéfice d"être parfois seul, pour comprendre notre entourage, et nous même au passage. Le loup des steppes de Herman Hesse. Pour les fous. Pour l"abîme. Pour le jeu des perspectives. Pour la complexité de ce que nous sommes, bien au-delà de la binarité qui empoisonne toutes choses, nous empêchant ainsi de regarder ailleurs. Mais je peux aussi flirter avec le cinéma. Ces vies sur grand écran. Cette échappatoire qu"est la sombre salle noire aux images colorées dans laquelle on s"engouffre pour deux ou trois heures. Parasite. Mélange de styles, c"est beau. C"est peut-être idiot, mais je pense qu"on ne peut pas dire beaucoup plus de choses. Au-delà d"un film, d"un instant, ce sont des sensations. Des sentiments. Des grimaces. Du vivant dans la salle de cinéma. La belle époque, Nicolas Bedos. Coloré, gigantesque. Vivant. Plus vivant que moi dans la salle de cinéma, mais qui nous offre l"envie de vivre plus, plus grand, plus mieux. Une ode aux erreurs qui nous rappellent le bonheur, des sortes de réveils à souvenirs, de réveils à avenir et à passé au milieu du temps présent.'),
(1, 'P2', 'Mais plus vivant encore que le cinéma, il y a le théâtre. Vous me direz, c"est une évidence, puisqu"au théâtre, les acteurs respirent devant nous. Ils incarnent un personnage le temps d"une soirée. Personnage qui existe pour eux dans les lignes apprises par cœur, dans les danses que l"on attribue, par les différentes manières de se mouvoir sur scène. Pourtant les personnages ont pour nous une connotation bien différente, ils vivent. Ils vivent en les acteurs, bien sûr. Mais au-delà de ça, c"est pour nos têtes de spectateurs que le personnage existe, reste, et prolonge son existence bien au-delà de la pièce. Six personnages en quête d"auteur. C"est une pièce qu"il faut lire. Pour son imagination. Pour sa réalité. Où peuvent bien se cacher tous les personnages qui prennent forme dans les textes qu"on lit, qu"on voit mis en scène, que l"on regarde en boucle grâce à la technologie de l"internet ou du DVD ? Mais où existent-ils sinon dans notre esprit farfelu d"humain aux besoins questionnables ? Ces personnages que l"on chérit et qui nous apportent tant dans la construction de nos idées, et de notre personne. Prenons le temps d"avoir une pensée pour ces êtres d"encre qui sont bien plus vivants qu"on ne le pense.'),
(1, 'Conclusion', 'Voilà mon introduction pour vous parler de Nos Solitudes, une pièce de Delphine Hecquet sur l’enfance et la famille, sur les non-dits, les oublis et les difficultés à être au milieu d’une famille, d’un monde, d’un microcosme qui nous est plus ou moins imposé. Comment la famille façonne l’adulte-enfant que nous devenons. Théâtre, chant, danse, ce n’est pourtant pas une comédie musicale. Ce n’est pas une comédie du tout d’ailleurs. Mais c’est beau, fou et émouvant. C’est extraordinaire ce que l’on peut faire avec du texte, et un corps.'),
(2, 'Texte article', 'La Ballade de Pern, saga d’une vingtaine de tomes appartient au genre plutôt méconnu de la « science-fantasy ». L’histoire se passe dans un futur lointain, sur Pern, une planète colonisée par les hommes, et ayant perdue tout contact avec la Terre depuis plusieurs milliers d’années. Le mode de vie de cette population fait appel à l’imaginaire moyenâgeux collectif. Une répartition de la population en Forts et Ateliers, une hiérarchie claire : apprentis, compagnons et maîtres, lords et dames de Forts… Le petit plus ? Les dragons. Les chevaliers-dragons et leurs montures majestueuses forment les Weyrs. Leur rôle: protéger Pern de la menace venue du ciel, les Fils, originaires de l’Etoile Rouge. Les Fils sont d’étranges filaments tombants en pluie qui dévorent toute matière organique avec laquelle ils entrent en contact. Si vous avez raté BL01 ce n’est pas grave, retenez juste qu’il vaut mieux ne pas être un organisme vivant en leur proximité, et que se cacher dans une maison en bois ne suffira pas. Dans « Le Vol du dragon », premier tome paru de la saga, cela fait 400 ans qu'il n’y a pas eu de Chute de Fils, les Weyrs sont tombés en désuétude, à quoi servent les dragons et leurs partenaires s’il n’y a plus de Fils à brûler ? C’est une évidence, les Fils ne tomberont plus, c’est la fin des Weyrs… A cela, F’lar, nouveau chef du Weyr de Benden, ne croit pas. Les cartes du ciel sont formelles, les Fils vont retomber, et bientôt ! Suivant les tomes on peut suivre l’histoire du point de vue de différents personnages, mais aussi dans diverses périodes historiques. Ainsi, on peut assister dans « La Dame aux dragons » à la lutte contre une épidémie, dans le « Chant du Dragon » à l’histoire de Menolly, rêvant de devenir Harpiste, ou dans « l’Aube des dragons » à l’arrivée et l’installation des colons sur Pern. À travers toutes ces histoires, l’Étoile Rouge brille maléfiquement dans le ciel, et les Fils dévorent tout sur leur passage.');

INSERT INTO Bloc_Image(article, titre, lien_image)
VALUES(2, 'Illustration livre', 'https://i.imgur.com/05ilBUE.jpg');
